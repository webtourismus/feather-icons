#!/usr/bin/env sh

ICON_SOURCE_DIR="./vendor/npm-asset/feather-icons/icons/"

echo ""
echo "Rebuilding custom dist of Feather icons"
echo ""

if [ ! -d "$ICON_SOURCE_DIR" ]; then
  echo "FEHLER"
  echo ""
  echo "Das Verzeichnis $ICON_SOURCE_DIR existiert nicht. Starte composer install --require-dev"
  echo ""
  exit 1
fi

rm -f ./*.svg

for FILE in ${ICON_SOURCE_DIR}*.svg;
do
  FILENAME=${FILE#$ICON_SOURCE_DIR}
  ICONNAME=${FILENAME%.svg}
  sed 's/viewBox=/class="svg svg--feather feather-'$ICONNAME'" viewBox=/; s/stroke-width="2"/stroke-width="1"/; /^  width="24"/d; /^  height="24"/d' ${FILE} > ./${STYLE_NAME}/${FILENAME}
done
